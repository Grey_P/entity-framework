﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Projects.Common.Models;
using System.Text.RegularExpressions;

namespace Projects.Client.Services
{
    class HttpService
    {
        private string PATH = "https://localhost:5001/api";


        // 1
        public async void GetNumOfTaskByAuthorID()
        {
            try
            {
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Projects/GetNumOfTaskByAuthorID/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("|Project id : Task Number|");
                    Console.WriteLine("Result:" + response.Content.ReadAsStringAsync().Result);
                }
            } catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 2
        public async void GetListOfTaskForPerformer()
        {
            try
            {
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Tasks/TaskForPerformer/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("Task id: description");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<TaskDTO> usersTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(json);
                    foreach (var task in usersTasks)
                    {
                        Console.WriteLine(task.Id + ": " + task.Description);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 3
        public async void GetFinishedTaskByUserID()
        {
            try{
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Tasks/Finished/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("Task id: name");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<IdNameOfTaskModel> usersTasks = JsonConvert.DeserializeObject<List<IdNameOfTaskModel>>(json);
                    if(usersTasks == null)
                    {
                        Console.WriteLine("Зроблений таскiв не знайдено");
                        return;
                    }
                    foreach (var task in usersTasks)
                    {
                        Console.WriteLine(task.Id + ": " + task.Name);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 4

        public async void GetTeamWhereOld()
        {
            try
            {
                var url = PATH + "/Teams/GetTeamWhereOldPeople";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("TeamID : name");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<TeamDTO> teams = JsonConvert.DeserializeObject<List<TeamDTO>>(json);
                    if (teams == null)
                    {
                        Console.WriteLine("Жодної команди не знайдено");
                        return;
                    }
                    foreach (var team in teams)
                    {
                        Console.WriteLine(team.Id + ": " + team.Name);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 5
        public async void GetPeopleWithTask()
        {
            try
            {
                var url = PATH + "/Users/GetPeopleWithTask";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("UserID+name : Number tasks");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<PeopleWithTasksModel> users = JsonConvert.DeserializeObject<List<PeopleWithTasksModel>>(json);
                    if (users == null)
                    {
                        Console.WriteLine("Жодного користувача не знайдено");
                        return;
                    }
                    foreach (var user in users)
                    {
                        Console.WriteLine("ID:" + user.User.Id+" | " + user.User.FirstName + ": " + user.Tasks.Count);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 6
        public async void GetUserInfo()
        {
            try
            {
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Users/GetUserInfo/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    UserInfoModel userInfo = JsonConvert.DeserializeObject<UserInfoModel>(json);
                    if (userInfo == null)
                    {
                        Console.WriteLine("Жодного користувача не знайдено");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("ID:" + userInfo.User.Id);
                        Console.WriteLine("Name:" + userInfo.User.FirstName + " " + userInfo.User.LastName);
                        if(userInfo.LastProject != null)
                        {
                            Console.WriteLine("Last project Name: " + userInfo.LastProject.Name);
                        }
                        if(userInfo.LongestTask != null)
                        {
                            Console.WriteLine("Longest task name: " + userInfo.LongestTask.Name);
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 7
        public async void GetProjectInfo()
        {
            try
            {
                var url = PATH + "/Projects/GetProjectInfo";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<ProjectInfoModel> projectsInfo = JsonConvert.DeserializeObject<List<ProjectInfoModel>>(json);
                    if (projectsInfo == null)
                    {
                        Console.WriteLine("Жодного проекту не знайдено");
                        return;
                    }
                    else
                    {
                        foreach(var proj in projectsInfo)
                        {
                            Console.WriteLine("Назва та id: " + proj.Project.Name + "_" + proj.Project.Id );
                            Console.WriteLine("Кiлькiсть користувачiв" + proj.UserNumber);
                            if(proj.ShorterTask != null)
                            {
                                Console.WriteLine("Найкоротший таск" + proj.ShorterTask.Name);
                            }
                            if(proj.LongestTask != null)
                            {
                                Console.WriteLine("Найдовший таск" + proj.LongestTask.Name);
                            }
                            Console.WriteLine("-----------------");
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 8

        public void AddUser()
        {
            try
            {
                var url = PATH + "/Users/AddUser";

                UserDTO userDTO = new UserDTO();
                Console.WriteLine("Введiть id команди");
                Console.Write("--> ");
                userDTO.TeamId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть iм'я");
                Console.Write("--> ");
                userDTO.FirstName = Console.ReadLine();

                Console.WriteLine("Введiть прiзвище");
                Console.Write("--> ");
                userDTO.LastName = Console.ReadLine();

                string mail;
                Console.WriteLine("Введiть email:");
                Console.Write("--> ");
                mail = Console.ReadLine();
                if (!Regex.IsMatch(mail, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                {
                    Console.WriteLine("Неправильний формат email");
                    return;
                }
                userDTO.Email = mail;


                Console.WriteLine("Введiть дату народження:");
                Console.Write("--> ");
                DateTime userBirthDay;
                if (DateTime.TryParse(Console.ReadLine(), out userBirthDay))
                {
                    userDTO.BirthDay = userBirthDay;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                userDTO.RegisteredAt = DateTime.Now;

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(userDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PostAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        //9

        public void UpdateUser()
        {
            try
            {
                var url = PATH + "/Users";

                UserDTO userDTO = new UserDTO();
                Console.WriteLine("Введiть id користувача");
                Console.Write("--> ");
                userDTO.Id = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть id команди");
                Console.Write("--> ");
                userDTO.TeamId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть iм'я");
                Console.Write("--> ");
                userDTO.FirstName = Console.ReadLine();

                Console.WriteLine("Введiть прiзвище");
                Console.Write("--> ");
                userDTO.LastName = Console.ReadLine();

                string mail;
                Console.WriteLine("Введiть email:");
                Console.Write("--> ");
                mail = Console.ReadLine();
                if (!Regex.IsMatch(mail, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                {
                    Console.WriteLine("Неправильний формат email");
                    return;
                }
                userDTO.Email = mail;


                Console.WriteLine("Введiть дату народження:");
                Console.Write("--> ");
                DateTime userBirthDay;
                if (DateTime.TryParse(Console.ReadLine(), out userBirthDay))
                {
                    userDTO.BirthDay = userBirthDay;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                userDTO.RegisteredAt = DateTime.Now;

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(userDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PutAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 10

        public void DeleteUser()
        {
            try
            {
                Console.WriteLine("Введiть id користувача");
                Console.Write("--> ");
                int TeamId = int.Parse(Console.ReadLine());
                var url = PATH + "/Users/" + TeamId;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response =  client.DeleteAsync(url);
                    Console.WriteLine("Status:" + response.Result.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 11

        public void AddTeam()
        {
            try
            {
                var url = PATH + "/Teams/AddTeam";

                TeamDTO teamDTO = new TeamDTO();
                Console.WriteLine("Введiть назву команди");
                Console.Write("--> ");
                teamDTO.Name = Console.ReadLine();
                teamDTO.CreatedAt = DateTime.Now;

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(teamDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PostAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 12 

        public void UpdateTeam()
        {
            try
            {
                var url = PATH + "/Teams";

                TeamDTO teamDTO = new TeamDTO();

                Console.WriteLine("Введiть id команди");
                Console.Write("--> ");
                teamDTO.Id = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть назву команди");
                Console.Write("--> ");
                teamDTO.Name = Console.ReadLine();

                teamDTO.CreatedAt = DateTime.Now;

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(teamDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PutAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 13

        public void DeleteTeam()
        {
            try
            {
                Console.WriteLine("Введiть id команди");
                Console.Write("--> ");
                int TeamId = int.Parse(Console.ReadLine());
                var url = PATH + "/Teams/" + TeamId;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = client.DeleteAsync(url);
                    Console.WriteLine("Status:" + response.Result.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        //14 
        public void AddProject()
        {
            try
            {
                var url = PATH + "/Projects/AddProject";

                ProjectDTO projectDTO = new ProjectDTO();

                Console.WriteLine("Введiть назву проекту");
                Console.Write("--> ");
                projectDTO.Name = Console.ReadLine();

                Console.WriteLine("Введiть id команди");
                Console.Write("--> ");
                projectDTO.TeamId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть id автора проекту");
                Console.Write("--> ");
                projectDTO.AuthorId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть опис проекту");
                Console.Write("--> ");
                projectDTO.Description = Console.ReadLine();

                Console.WriteLine("Введiть дату створення проекту");
                Console.Write("--> ");
                DateTime projectCreated;
                if (DateTime.TryParse(Console.ReadLine(), out projectCreated))
                {
                    projectDTO.CreatedAt = projectCreated;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                Console.WriteLine("Введiть deadline проекту");
                Console.Write("--> ");
                DateTime deadline;
                if (DateTime.TryParse(Console.ReadLine(), out deadline))
                {
                    projectDTO.Deadline = deadline;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(projectDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PostAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 15

        public void UpdateProject()
        {
            try
            {
                var url = PATH + "/Projects";

                ProjectDTO projectDTO = new ProjectDTO();

                Console.WriteLine("Введiть id проекту, для якого оновлюємо iнформацiю");
                Console.Write("--> ");
                projectDTO.Id = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть назву проекту");
                Console.Write("--> ");
                projectDTO.Name = Console.ReadLine();

                Console.WriteLine("Введiть id команди");
                Console.Write("--> ");
                projectDTO.TeamId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть id автора проекту");
                Console.Write("--> ");
                projectDTO.AuthorId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть опис проекту");
                Console.Write("--> ");
                projectDTO.Description = Console.ReadLine();

                Console.WriteLine("Введiть дату створення проекту");
                Console.Write("--> ");
                DateTime projectCreated;
                if (DateTime.TryParse(Console.ReadLine(), out projectCreated))
                {
                    projectDTO.CreatedAt = projectCreated;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                Console.WriteLine("Введiть deadline проекту");
                Console.Write("--> ");
                DateTime deadline;
                if (DateTime.TryParse(Console.ReadLine(), out deadline))
                {
                    projectDTO.Deadline = deadline;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(projectDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PutAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 16

        public void DeleteProject()
        {
            try
            {
                Console.WriteLine("Введiть id проекту");
                Console.Write("--> ");
                int projectId = int.Parse(Console.ReadLine());
                var url = PATH + "/Projects/" + projectId;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = client.DeleteAsync(url);
                    Console.WriteLine("Status:" + response.Result.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 17

        public void AddTask()
        {
            try
            {
                var url = PATH + "/Tasks/AddTask";

                TaskDTO taskDTO = new TaskDTO();

                Console.WriteLine("Введiть назву таску");
                Console.Write("--> ");
                taskDTO.Name = Console.ReadLine();

                Console.WriteLine("Введiть id проекту");
                Console.Write("--> ");
                taskDTO.ProjectId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть id розробника(хто буде вирiшувати)");
                Console.Write("--> ");
                taskDTO.PerformerId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть state");
                Console.Write("--> ");
                taskDTO.State = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть опис завдання");
                Console.Write("--> ");
                taskDTO.Description = Console.ReadLine();

                Console.WriteLine("Введiть дату створення таску");
                Console.Write("--> ");
                DateTime taskCreated;
                if (DateTime.TryParse(Console.ReadLine(), out taskCreated))
                {
                    taskDTO.CreatedAt = taskCreated;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                Console.WriteLine("Введiть кiнцеву дату таску");
                Console.Write("--> ");
                DateTime taskFinished;
                if (DateTime.TryParse(Console.ReadLine(), out taskFinished))
                {
                    taskDTO.CreatedAt = taskFinished;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(taskDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PostAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 18

        public void UpdateTask()
        {
            try
            {
                var url = PATH + "/Tasks";

                TaskDTO taskDTO = new TaskDTO();

                Console.WriteLine("Введiть id таску");
                Console.Write("--> ");
                taskDTO.Id = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть назву таску");
                Console.Write("--> ");
                taskDTO.Name = Console.ReadLine();

                Console.WriteLine("Введiть id проекту");
                Console.Write("--> ");
                taskDTO.ProjectId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть id розробника(хто буде вирiшувати)");
                Console.Write("--> ");
                taskDTO.PerformerId = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть state");
                Console.Write("--> ");
                taskDTO.State = int.Parse(Console.ReadLine());

                Console.WriteLine("Введiть опис завдання");
                Console.Write("--> ");
                taskDTO.Description = Console.ReadLine();

                Console.WriteLine("Введiть дату створення таску");
                Console.Write("--> ");
                DateTime taskCreated;
                if (DateTime.TryParse(Console.ReadLine(), out taskCreated))
                {
                    taskDTO.CreatedAt = taskCreated;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                Console.WriteLine("Введiть кiнцеву дату таску");
                Console.Write("--> ");
                DateTime taskFinished;
                if (DateTime.TryParse(Console.ReadLine(), out taskFinished))
                {
                    taskDTO.CreatedAt = taskFinished;
                }
                else
                {
                    Console.WriteLine("Неправильний формат дати.");
                }

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var json = JsonConvert.SerializeObject(taskDTO);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var responce = client.PutAsync(url, data).Result;
                    Console.WriteLine("Status code: " + responce.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 19

        public void DeleteTask()
        {
            try
            {
                Console.WriteLine("Введiть id таску");
                Console.Write("--> ");
                int projectId = int.Parse(Console.ReadLine());
                var url = PATH + "/Tasks/" + projectId;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = client.DeleteAsync(url);
                    Console.WriteLine("Status:" + response.Result.StatusCode);
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        //********** Part for bypassing the certificate :)
        public HttpClientHandler BypassCertificate()
        {

            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            return clientHandler;

        }
        //***************************************************
    }
}
