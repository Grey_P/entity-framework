﻿using System;
using System.Collections.Generic;
using System.Text;
using Projects.Client.Interfaces;

namespace Projects.Client.Services
{
    
    public class MenuService : IMenu
    {
        HttpService httpService = new HttpService();
        private bool exit = true;
        public void Start()
        {
            while (exit)
            {
                Menuintroduction();
            }
        }

        public void Menuintroduction()
        {
            Console.WriteLine("-----< Hello! >-----");
            Console.WriteLine("Please, chose action:");
            Console.WriteLine("1.Отримати кiлькiсть таскiв у проектi конкретного користувача (по id).");
            Console.WriteLine("2.Отримати список таскiв, призначених для конкретного користувача (по id).");
            Console.WriteLine("3.Отримати список з колекцiї таскiв, якi виконанi в поточному роцi для конкретного користувача (по id).");
            Console.WriteLine("4.Отримати список з колекцiї команд, учасники яких старшi 10 рокiв.");
            Console.WriteLine("5.Отримати список користувачiв за алфавiтом first_name.");
            Console.WriteLine("6.Отримати iнформацiю по користувачу(Останнiй проект, незавершенi таски).");
            Console.WriteLine("7.Отримати iнформацiю про всi проекти.");
            Console.WriteLine("8.Додати User.");
            Console.WriteLine("9.Оновити iнформацiю по User.");
            Console.WriteLine("10.Видалити User(по id)");
            Console.WriteLine("11.Додати Team");
            Console.WriteLine("12.Оновити iнформацiю по Team.");
            Console.WriteLine("13.Видалити Team(по id)");

            Console.WriteLine("14.Додати Project");
            Console.WriteLine("15.Оновити iнформацiю по Prokect.");
            Console.WriteLine("16.Видалити Project(по id)");

            Console.WriteLine("17.Додати Task");
            Console.WriteLine("18.Оновити iнформацiю по Task.");
            Console.WriteLine("19.Видалити Task(по id)");

            Console.WriteLine("0.Завершити сеанс.");
            Console.Write("Chose number--->  ");
            var val = Console.ReadLine();
            try
            {
                int a = int.Parse(val);
                if (a >= 0 && a <= 19)
                {
                    Console.Clear();
                    switch (a)
                    {
                        case 1:
                            // client.AddTransportToParkingAsync();
                            httpService.GetNumOfTaskByAuthorID();
                            Console.ReadKey();
                            break;
                        case 2:
                            httpService.GetListOfTaskForPerformer();
                            Console.ReadKey();
                            break;
                        case 3:
                            httpService.GetFinishedTaskByUserID();
                            Console.ReadKey();
                            break;
                        case 4:
                            httpService.GetTeamWhereOld();
                            Console.ReadKey();
                            break;
                        case 5:
                            httpService.GetPeopleWithTask();
                            Console.ReadKey();
                            break;
                        case 6:
                            // NOTE: if user doesn`t have project
                            // will be 204 no content
                            httpService.GetUserInfo();
                            Console.ReadKey();
                            break;
                        case 7:
                            httpService.GetProjectInfo();
                            Console.ReadKey();
                            break;
                        case 8:
                            httpService.AddUser();
                            Console.ReadKey();
                            break;
                        case 9:
                            httpService.UpdateUser();
                            Console.ReadKey();
                            break;
                        case 10:
                            httpService.DeleteUser();
                            Console.ReadKey();
                            break;
                        case 11:
                            httpService.AddTeam();
                            Console.ReadKey();
                            break;
                        case 12:
                            httpService.UpdateTeam();
                            Console.ReadKey();
                            break;
                        case 13:
                            httpService.DeleteTeam();
                            Console.ReadKey();
                            break;
                        case 14:
                            httpService.AddProject();
                            Console.ReadKey();
                            break;
                        case 15:
                            httpService.UpdateProject();
                            Console.ReadKey();
                            break;
                        case 16:
                            httpService.DeleteProject();
                            Console.ReadKey();
                            break;

                        case 17:
                            httpService.AddTask();
                            Console.ReadKey();
                            break;
                        case 18:
                            httpService.UpdateTask();
                            Console.ReadKey();
                            break;
                        case 19:
                            httpService.DeleteTask();
                            Console.ReadKey();
                            break;


                        case 0:
                            Stop();
                            break;

                    }
                    Console.Clear();

                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Next time enter number from 1 to 9 pleeeease :)");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            catch
            {
                Console.Clear();
                Console.WriteLine("Incorrect input :( ");
                Console.ReadKey();
                Console.Clear();
                return;
            }
        }

        public void Stop()
        {
            exit = false;
            Console.WriteLine("Good luck)))");
            Console.ReadKey();
        }
    }
}
