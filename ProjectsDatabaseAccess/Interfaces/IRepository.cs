﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.DatabaseAccess.Interfaces
{ 
    public interface IRepository<TypeModel>
    {  
        IEnumerable<TypeModel> GetAll();
        void Add(TypeModel elem);
        void Update(TypeModel elem);
        void Delete(TypeModel elem);
        void SaveChanges();
    }
}
