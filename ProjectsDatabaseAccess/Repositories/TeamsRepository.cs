﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Projects.DatabaseAccess.Entities;
using Newtonsoft.Json;
using Projects.DatabaseAccess;
using Microsoft.EntityFrameworkCore;

namespace Projects.DatabaseAccess.Repositories
{
    public class TeamsRepository : GenericRepository<TeamModel>
    {
      //  DbContext DbContext;
      //  DbSet<ProjectModel> DbSet;
        public TeamsRepository(ProjectsDbContext context) : base(context)
        {
            this._DbContext = context;
            this._dbSet = context.Set<TeamModel>();
        }

        override public void Update(TeamModel team)
        {
            _dbSet.Where(el => el.Id == team.Id).FirstOrDefault().Name = team.Name;
            _dbSet.Where(el => el.Id == team.Id).FirstOrDefault().CreatedAt = team.CreatedAt;
            _DbContext.SaveChanges();
        }
    }
}
