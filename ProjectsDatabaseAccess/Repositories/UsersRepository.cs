﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
//using Projects.WebAPI.Models;
using Projects.DatabaseAccess.Entities;
using Newtonsoft.Json;
using Projects.DatabaseAccess;

namespace Projects.DatabaseAccess.Repositories
{
    public class UsersRepository : GenericRepository<UserModel>
    {
      //  DbContext DbContext;
      //  DbSet<ProjectModel> DbSet;
        public UsersRepository(ProjectsDbContext context) : base(context)
        {
            this._DbContext = context;
            this._dbSet = context.Set<UserModel>();
        }

        override public void Update(UserModel user)
        {
            _dbSet.Where(el => el.Id == user.Id).FirstOrDefault().BirthDay = user.BirthDay;
            _dbSet.Where(el => el.Id == user.Id).FirstOrDefault().Email = user.Email;
            _dbSet.Where(el => el.Id == user.Id).FirstOrDefault().FirstName = user.FirstName;
            _dbSet.Where(el => el.Id == user.Id).FirstOrDefault().LastName = user.LastName;
            _dbSet.Where(el => el.Id == user.Id).FirstOrDefault().RegisteredAt = user.RegisteredAt;
            _dbSet.Where(el => el.Id == user.Id).FirstOrDefault().TeamId = user.TeamId;
            _DbContext.SaveChanges();
        }
    }
}
