﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Data.Entity;
using System.Linq.Expressions;
using Projects.DatabaseAccess.Interfaces;
using Projects.DatabaseAccess;
using Microsoft.EntityFrameworkCore;

namespace Projects.DatabaseAccess.Repositories
{
    public abstract class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        public ProjectsDbContext _DbContext;
        public DbSet<TEntity> _dbSet;

        public GenericRepository(ProjectsDbContext context)
        {
        }  

        public void Add(TEntity elem)
        {
            _dbSet.Add(elem);
            SaveChanges();
        }

        public void Delete(TEntity elem)
        {
            _dbSet.Remove(elem);
            SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public void SaveChanges()
        {
            _DbContext.SaveChanges();
        }

        public virtual void Update(TEntity elem)
        {
            // It doesn`t work, so I override this method for every repository
            /*_DbContext.Set<TEntity>().Attach(elem);
            _DbContext.Entry(elem).State = EntityState.Modified;
            _DbContext.SaveChangesAsync().ConfigureAwait(false);*/
        }
    }
}
