﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

//using Projects.WebAPI.Models;
using Projects.DatabaseAccess.Entities;

using Newtonsoft.Json;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Projects.DatabaseAccess;

namespace Projects.DatabaseAccess.Repositories
{
    public class TasksRepository : GenericRepository<TaskModel>
    {
        public TasksRepository(ProjectsDbContext context) : base(context)
        {
            base._DbContext = context;
            base._dbSet = context.Set<TaskModel>();
        }

        override public void Update(TaskModel task)
        {
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().CreatedAt = task.CreatedAt;
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().Description = task.Description;
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().FinishedAt = task.FinishedAt;
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().Name = task.Name;
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().PerformerId = task.PerformerId;
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().ProjectId = task.ProjectId;
            _dbSet.Where(el => el.Id == task.Id).FirstOrDefault().State = task.State;
            _DbContext.SaveChanges();
        }
    }
}
