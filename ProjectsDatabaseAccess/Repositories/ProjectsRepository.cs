﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Projects.DatabaseAccess.Entities;
using Microsoft.EntityFrameworkCore;
//using System.Data.Entity;
using Projects.DatabaseAccess;

namespace Projects.DatabaseAccess.Repositories
{
    public class ProjectsRepository : GenericRepository<ProjectModel>
    {
      //  DbContext DbContext;
     //   DbSet<ProjectModel> DbSet;
        public ProjectsRepository(ProjectsDbContext context) : base(context)
        {
            this._DbContext = context;
            this._dbSet = context.Set<ProjectModel>();
        }

        public ProjectModel GetByID(int id)
        {
            return base._dbSet.Where(el => el.Id == id).FirstOrDefault();
        }

        public override void Update(ProjectModel project)
        {
            _dbSet.Where(el => el.Id == project.Id).FirstOrDefault().AuthorId = project.AuthorId;
            _dbSet.Where(el => el.Id == project.Id).FirstOrDefault().CreatedAt = project.CreatedAt;
            _dbSet.Where(el => el.Id == project.Id).FirstOrDefault().Deadline = project.Deadline;
            _dbSet.Where(el => el.Id == project.Id).FirstOrDefault().Description = project.Description;
            _dbSet.Where(el => el.Id == project.Id).FirstOrDefault().Name = project.Name;
            _dbSet.Where(el => el.Id == project.Id).FirstOrDefault().TeamId = project.TeamId;
            _DbContext.SaveChanges();
        }
    }
}
