﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Projects.DatabaseAccess.Entities;
using System.IO;
using Newtonsoft.Json;
using Projects.DatabaseAccess.DataStorageInJson;
using System.Linq;

namespace Projects.DatabaseAccess
{
    public class ProjectsDbContext : DbContext
    {
        public ProjectsDbContext(DbContextOptions<ProjectsDbContext> options ) : base(options)
        {
            
        }

        public DbSet<ProjectModel> Projects { get; set; }
        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<TeamModel> Teams { get; set; }
        public DbSet<UserModel> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            IEnumerable<ProjectModel> projects;
            IEnumerable<TaskModel> tasks;
            IEnumerable<TeamModel> teams;
            IEnumerable<UserModel> users;
            //----------------------------------
            string path = PathsToData.PathToProjectsJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                projects = JsonConvert.DeserializeObject<IEnumerable<ProjectModel>>(json);
            }
            //-----------------------------------
            path = PathsToData.PathToTasksJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                tasks = JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(json);
            }
            //--------------------------------------
            path = PathsToData.PathToTeamsJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                teams = JsonConvert.DeserializeObject<IEnumerable<TeamModel>>(json);
            }
            //-----------------------------------------
            path = PathsToData.PathToUsersJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                users = JsonConvert.DeserializeObject<IEnumerable<UserModel>>(json);
            }   

            modelBuilder.Entity<ProjectModel>().HasData(projects);
            modelBuilder.Entity<TeamModel>().HasData(teams);
            modelBuilder.Entity<TaskModel>().HasData(tasks);
            modelBuilder.Entity<UserModel>().HasData(users);

            base.OnModelCreating(modelBuilder);
        }
    }
}
