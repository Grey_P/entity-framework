﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Projects.DatabaseAccess.Entities
{
    public class UserModel
    {
         [Key]
         public int? Id { get; set; }
         public int? TeamId { get; set; }
         [Required(ErrorMessage = "There isn`t first name of user")]
         public string FirstName { get; set; }

        [Required(ErrorMessage = "There isn`t last name of user")]
        public string LastName { get; set; }

        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
         ErrorMessage = "Characters are not allowed.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "There isn`t data of registration")]
        public DateTime RegisteredAt { get; set; }

         public DateTime BirthDay { get; set; }
    }
}
