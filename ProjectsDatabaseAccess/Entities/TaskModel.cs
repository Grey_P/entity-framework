﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Projects.DatabaseAccess.Entities
{
    public class TaskModel
    {
        [Key]
        public int? Id { get; set; }
        [Required(ErrorMessage = "There isn`t project id")]
        public int ProjectId { get; set; }
        [Required(ErrorMessage = "There isn`t perforner id")]
        public int PerformerId { get; set; }
        [MaxLength(400)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int State { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }
        public UserModel Performer { get; set; }
    }
}
