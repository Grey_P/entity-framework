﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Projects.DatabaseAccess.Entities
{
    public class TeamModel
    {
        [Key]
        public int? Id { get; set; }
        [Required(ErrorMessage = "There isn`t team name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "There isn`t data of cteating")]
        public DateTime CreatedAt { get; set; }
    }
}
