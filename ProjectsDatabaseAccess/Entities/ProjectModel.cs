﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Projects.DatabaseAccess.Entities
{
    public class ProjectModel
    {
        [Key]
        public int? Id { get; set; }
        [Required(ErrorMessage = "There isn`t author id")]
        public int AuthorId { get; set; }
        [Required(ErrorMessage = "There isn`t team id")]
        public int TeamId { get; set; }
        [Required(ErrorMessage = "There isn`t name for project")]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<TaskModel> Tasks = new List<TaskModel>();
        public UserModel Author { get; set; }
        public TeamModel Team { get; set; }
    }
}
