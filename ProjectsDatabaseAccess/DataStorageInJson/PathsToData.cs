﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.DatabaseAccess.DataStorageInJson
{
    public static class PathsToData
    {
        public static string PathToProjectsJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.Parent.FullName + "/ProjectsDatabaseAccess/DataStorageInJson/Projects.json";
        public static string PathToTasksJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.Parent.FullName + "/ProjectsDatabaseAccess/DataStorageInJson/Tasks.json";
        public static string PathToTeamsJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.Parent.FullName + "/ProjectsDatabaseAccess/DataStorageInJson/Teams.json";
        public static string PathToUsersJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.Parent.FullName + "/ProjectsDatabaseAccess/DataStorageInJson/Users.json";
    }
}
