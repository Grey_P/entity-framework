﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;

namespace Projects.Common.Models
{
    public class ProjectInfoModel
    {
        public ProjectModel Project { get; set; }
        public TaskModel LongestTask { get; set; }
        public TaskModel ShorterTask { get; set; }
        public int UserNumber { get; set; }
    }
}
