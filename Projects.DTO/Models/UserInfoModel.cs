﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;

namespace Projects.Common.Models
{
    public class UserInfoModel
    {
        // I thought about LastProjectID and LongestTaskId...
        public UserModel User { get; set; }
        public ProjectModel LastProject { get; set; }
        public int TaskNumberForLastProject { get; set; }
        public int UndoneTasksNumber { get; set; }
        public TaskModel LongestTask { get; set; }
    }
}
