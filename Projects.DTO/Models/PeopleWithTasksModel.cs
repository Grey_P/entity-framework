﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;

namespace Projects.Common.Models
{
    public class PeopleWithTasksModel
    {
        public UserModel User { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
