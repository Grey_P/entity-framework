﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;
using Projects.DatabaseAccess.Interfaces;

namespace Projects.BLL.Services
{
    // This Service work with data of all Repositories, but hasn`t enough 
    // functional for be UOW. 
    // He can:
    // -- take data from Repo and move it to right view
    // -- give (formated) data for other services
    // -- give new or updated element to Repo.
    public class DataService
    {
        // Data inside
        IEnumerable<ProjectModel> _projects;
        IEnumerable<TaskModel> _tasks;
        IEnumerable<TeamModel> _teams;
        IEnumerable<UserModel> _users;

        // Repositories
        IRepository<ProjectModel> _projectRepository;
        IRepository<TaskModel> _tasksRepository;
        IRepository<TeamModel> _teamsRepository;
        IRepository<UserModel> _usersRepository;

        public DataService(IRepository<ProjectModel> ProjectRep, 
                                  IRepository<TaskModel> TasksRep,
                                  IRepository<TeamModel> TeamsRep,
                                  IRepository<UserModel> UsersRep)
        {
            this._projectRepository = ProjectRep;
            this._tasksRepository = TasksRep;
            this._teamsRepository = TeamsRep;
            this._usersRepository = UsersRep;

            UpdateInsideData();
        }

        //---------------------------
        // Moving (inside) data to right view
        private void UpdateInsideData()
        {
            this._projects = _projectRepository.GetAll();
            this._tasks = _tasksRepository.GetAll();
            this._teams = _teamsRepository.GetAll();
            this._users = _usersRepository.GetAll();
            // ----------------------------
            // Join performer to every task
            // 
            _tasks = _tasks.Join(_users,
                               t => t.PerformerId,
                               u => u.Id,
                               (t, u) =>
                               {
                                   t.Performer = u;
                                   return t;
                               }).ToList();
            //----------------------------
            // Join Tasks to every Project
            _projects = _projects.GroupJoin(_tasks,
                                    p => p.Id,
                                    t => t.ProjectId,
                                    (p, t) =>
                                    {
                                        p.Tasks = p.Tasks.Union(t.Select(b => b).ToList()).ToList();
                                        return p;
                                    }).ToList();

            //----------------------------------------
            // Join Author to every Project
            _projects = _projects.Join(_users,
                                    p => p.AuthorId,
                                    u => u.Id,
                                    (p, t) =>
                                    {
                                        p.Author = t;
                                        return p;
                                    }).ToList();

            //----------------------------------------
            // Join Team to every Project
            _projects = _projects.Join(_teams,
                                    p => p.TeamId,
                                    t => t.Id,
                                    (p, t) =>
                                    {
                                        p.Team = t;
                                        return p;
                                    }).ToList();

            // <--- At the moment we have structure like we were supposed
            // _Project
            //  |_Tasks
            //      |___Performer
            //  |__Author
            //  |__Team
        }

        //---------------------------------
        public IEnumerable<UserModel> GetUsers()
        {
            return _users;
        }
        public IEnumerable<ProjectModel> GetProjects()
        {
            return _projects;
        }
        public IEnumerable<TaskModel> GetTasks()
        {
            return _tasks;
        }
        public IEnumerable<TeamModel> GetTeams()
        {
            return _teams;
        }
        //-----------------------------------
        public void AddUser(UserModel user)
        {
            _usersRepository.Add(user);
            _users = _usersRepository.GetAll();
            UpdateInsideData();
        }
        public void AddProject(ProjectModel project)
        {
            _projectRepository.Add(project);
            _projects = _projectRepository.GetAll();
            UpdateInsideData();
        }
        public void AddTask(TaskModel task)
        {
            _tasksRepository.Add(task);
            _tasks = _tasksRepository.GetAll();
            UpdateInsideData();
        }
        public void AddTeam(TeamModel team)
        {
            _teamsRepository.Add(team);
            _teams = _teamsRepository.GetAll();
            UpdateInsideData();
        }
        //-----------------------------------------
        public void DeleteUser(int id)
        {

            UserModel user = _users.Where(el => el.Id == id).FirstOrDefault();
            if (user== null)
            {
                throw new ObjectNotFoundException();
            }
            _usersRepository.Delete(user);
            UpdateInsideData();
        }
        public void DeleteTeam(int id)
        {
            TeamModel item = _teams.Where(el => el.Id == id).FirstOrDefault();
            if(item == null){
                throw new ObjectNotFoundException();
            }
            _teamsRepository.Delete(item);
            UpdateInsideData();
        }
        public void DeleteTask(int id)
        {
            TaskModel item = _tasks.Where(el => el.Id == id).FirstOrDefault();
            if (item == null)
            {
                throw new ObjectNotFoundException();
            }
            _tasksRepository.Delete(item);
            UpdateInsideData();
        }
        public void DeleteProject(int id)
        {
            ProjectModel item = _projects.Where(el => el.Id == id).FirstOrDefault();
            if (item == null)
            {
                throw new ObjectNotFoundException();
            }
            _projectRepository.Delete(item);
            UpdateInsideData();
        }
        //----------------------------------

        public void UpdateUser(UserModel us)
        {
            _usersRepository.Update(us);
            _users = _usersRepository.GetAll();
            UpdateInsideData();
        }
        public void UpdateTask(TaskModel task)
        {
            _tasksRepository.Update(task);
            _tasks = _tasksRepository.GetAll();
            UpdateInsideData();
        }
        public void UpdateTeam(TeamModel team)
        {
            _teamsRepository.Update(team);
            _teams = _teamsRepository.GetAll();
            UpdateInsideData();
        }
        public void UpdateProject(ProjectModel proj)
        {
            _projectRepository.Update(proj);
            _projects = _projectRepository.GetAll();
            UpdateInsideData();
        }
    }
}
