﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.Common.Models;
using Projects.DatabaseAccess.Entities;

namespace Projects.BLL.Services
{
    public static class ConverterService
    {
        // Service created for converting Model to DTO model
        public static ProjectDTO ProjectModelToDTO(ProjectModel pm)
        {
            ProjectDTO dto = new ProjectDTO();
            dto.Id = pm.Id;
            dto.Name = pm.Name;
            dto.TeamId = pm.TeamId;
            dto.Description = pm.Description;
            dto.AuthorId = pm.AuthorId;
            dto.CreatedAt = pm.CreatedAt;
            dto.Deadline = pm.Deadline;
            return dto;
        }

        public static ProjectModel ProjectDTOToModel(ProjectDTO dto)
        {
            ProjectModel pm = new ProjectModel();
            pm.Id = dto.Id;
            pm.Name = dto.Name;
            pm.TeamId = dto.TeamId;
            pm.Description = dto.Description;
            pm.AuthorId = dto.AuthorId;
            pm.CreatedAt = dto.CreatedAt;
            pm.Deadline = dto.Deadline;
            return pm;
        }

        public static UserDTO UserModelToDTO(UserModel um)
        {
            UserDTO dto = new UserDTO();
            dto.BirthDay = um.BirthDay;
            dto.Email = um.Email;
            dto.Id = um.Id;
            dto.LastName = um.LastName;
            dto.RegisteredAt = um.RegisteredAt;
            dto.TeamId = um.TeamId;
            dto.FirstName = um.FirstName;
            return dto;
        }

        public static UserModel UserDTOToModel(UserDTO dto)
        {
            UserModel um = new UserModel();
            um.BirthDay = dto.BirthDay;
            um.Email = dto.Email;
            um.Id = dto.Id;
            um.LastName = dto.LastName;
            um.RegisteredAt = dto.RegisteredAt;
            um.TeamId = dto.TeamId;
            um.FirstName = dto.FirstName;
            return um;
        }

        public static TeamDTO TeamModelToDTO(TeamModel tm)
        {
            TeamDTO dto = new TeamDTO();
            dto.CreatedAt = tm.CreatedAt;
            dto.Id = tm.Id;
            dto.Name = tm.Name;
            return dto;
        }

        public static TeamModel TeamDTOToModel(TeamDTO dto)
        {
            TeamModel tm = new TeamModel();
            tm.CreatedAt = dto.CreatedAt;
            tm.Id = dto.Id;
            tm.Name = dto.Name;
            return tm;
        }

        public static TaskDTO TaskModelToDTO(TaskModel tm)
        {
            TaskDTO dto = new TaskDTO();
            dto.Id = tm.Id;
            dto.ProjectId = tm.ProjectId;
            dto.PerformerId = tm.PerformerId;
            dto.Name = tm.Name;
            dto.Description = tm.Description;
            dto.State = tm.State;
            dto.CreatedAt = tm.CreatedAt;
            dto.FinishedAt = tm.FinishedAt;
            return dto;
        }

        public static TaskModel TaskDTOToModel(TaskDTO dto)
        {
            TaskModel tm = new TaskModel();
            tm.Id = dto.Id;
            tm.ProjectId = dto.ProjectId;
            tm.PerformerId = dto.PerformerId;
            tm.Name = dto.Name;
            tm.Description = dto.Description;
            tm.State = dto.State;
            tm.CreatedAt = dto.CreatedAt;
            tm.FinishedAt = dto.FinishedAt;
            return tm;
        }
    }
}
