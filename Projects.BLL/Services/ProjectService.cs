﻿using Projects.DatabaseAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.DatabaseAccess;
using Projects.BLL.Interfaces;
using Projects.Common.Models;

namespace Projects.BLL.Services
{
    public class ProjectService : ITableService<ProjectModel>
    {
        private DataService _dataService;
        public ProjectService(DataService ds)
        {
            this._dataService = ds;
        }
        public void Add(ProjectModel project)
        {
            _dataService.AddProject(project);
        }

        public void Delete(int id)
        {
            _dataService.DeleteProject(id);
        }

        public IEnumerable<ProjectModel> Get()
        {
            return _dataService.GetProjects();
        }

        public void Update(ProjectModel project)
        {
            _dataService.UpdateProject(project);
        }

        //--------------------------------------
        // from LINQ lection Task 1
        // Отримати кількість тасків у проекті конкретного користувача 
        // (по id) (словник, де key буде проект, а value кількість тасків).
        public IDictionary<int, int> GetNumOfTaskByAuthorID(int id)
        {
            return _dataService.GetProjects().Where(p => p.AuthorId == id)
                .Select(p => new {key = p.Id ?? default(int), value = p.Tasks.Count }).
                Distinct().ToDictionary(result => result.key, result => result.value);
        }

        //-----------------------------------------------
        // Task 7
        // Отримати таку структуру: Проект - Найдовший таск проекту (за описом) -
        // Найкоротший таск проекту (по імені) - Загальна кількість користувачів в команді проекту,
        // де або опис проекту >20 символів, або кількість тасків <3
        public IEnumerable<ProjectInfoModel> GetProjectInfo()
        {
            return _dataService.GetProjects().Select(p => new ProjectInfoModel()
            {
                Project = p,
                LongestTask = p.Tasks.OrderByDescending(el => el.Description.Length).FirstOrDefault(),
                ShorterTask = p.Tasks.OrderBy(el => el.Name.Length).FirstOrDefault(),
                UserNumber = _dataService.GetUsers().Where(user => user.TeamId == p.TeamId
                && (p.Description.Length > 20
                || p.Tasks.Count < 3)).Count()
            }).ToList();
        }

    }
}
