﻿using Projects.BLL.Interfaces;
using Projects.DatabaseAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class TeamsService : ITableService<TeamModel>
    {
        private DataService _dataService;
        public TeamsService(DataService ds)
        {
            this._dataService = ds;
        }
        public void Add(TeamModel el)
        {
            _dataService.AddTeam(el);
        }

        public void Delete(int elid)
        {
            _dataService.DeleteTeam(elid);
        }

        public IEnumerable<TeamModel> Get()
        {
            return _dataService.GetTeams();
        }

        public void Update(TeamModel el)
        {
            _dataService.UpdateTeam(el);
        }

        //---------------------------------------
        // from LINQ lection Task 4
        // Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        // відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        public IEnumerable<TeamModel> GetTeamPeopleOlderThen10()
        {
            return _dataService.GetUsers().OrderByDescending(u => u.RegisteredAt).
                          GroupBy(u => u.TeamId).
                          Where(u => u.All(a => DateTime.Now.Year - a.BirthDay.Year >= 10)).
                          Join(_dataService.GetTeams(),
                          u => u.Key,
                          t => t.Id,
                          (u, t) => { return t; }).ToList();
        }
    }
}
