﻿using Projects.BLL.Interfaces;
using Projects.DatabaseAccess.Entities;
using Projects.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class TasksService : ITableService<TaskModel>
    {
        private DataService _dataService;
        public TasksService(DataService ds)
        {
            this._dataService = ds;
        }

        public void Add(TaskModel task)
        {
            _dataService.AddTask(task);
        }

        public void Delete(int id)
        {
            _dataService.DeleteTask(id);
        }

        public IEnumerable<TaskModel> Get()
        {
            return _dataService.GetTasks();
        }

        public void Update(TaskModel task)
        {
            _dataService.UpdateTask(task);
        }

        //----------------------------------------
        // from LINQ lection Task2
        // Отримати список тасків, призначених для конкретного користувача (по id), 
        // де name таска <45 символів (колекція з тасків).
        public ICollection<TaskModel> GetListOfTaskForPerformer(int id)
        {
            return _dataService.GetTasks().Where(t => t.PerformerId == id).Where(t => t.Name.Length < 45).Select(t => t).ToList();
        }

        //---------------------------------------
        // from LINQ lection Task 3
        // Отримати список (id, name) з колекції тасків, які виконані (finished)
        // в поточному (2021) році для конкретного користувача (по id).
        public IEnumerable<IdNameOfTaskModel> GetFinishedTaskByUserID(int Authorid)
        {
            return _dataService.GetTasks().Where(t => t.PerformerId == Authorid)
                .Where(t => t.FinishedAt != null && t.FinishedAt.Value.Year == DateTime.Now.Year)
                .Select(t => new IdNameOfTaskModel() { Id = t.Id, Name = t.Name }).ToList();
        }
    }
}
