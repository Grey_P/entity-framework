﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.BLL.Interfaces;
using Projects.DatabaseAccess.Entities;
using Projects.Common.Models;

namespace Projects.BLL.Services
{
    public class UsersService : ITableService<UserModel>
    {
        private DataService _dataService;
        public UsersService(DataService ds)
        {
            this._dataService = ds;
        }
        public void Add(UserModel el)
        {
            el.RegisteredAt = DateTime.Now;
            _dataService.AddUser(el);
        }

        public void Delete(int elemID)
        {
            _dataService.DeleteUser(elemID);
        }

        public IEnumerable<UserModel> Get()
        {
            return _dataService.GetUsers();
        }

        public void Update(UserModel el)
        {
            _dataService.UpdateUser(el);
        }

        //--------------------------------------
        // From LINQ lection Task 5
        // Отримати список користувачів за алфавітом first_name (по зростанню) 
        // з відсортованими tasks по довжині name (за спаданням).
        public IEnumerable<PeopleWithTasksModel> GetPeopleWithTask()
        {
            return (_dataService.GetTasks()).OrderByDescending(t => t.Name.Length).
                          GroupBy(t => t.PerformerId).
                          Join(_dataService.GetUsers(),
                          t => t.Key,
                          u => u.Id,
                          (t, u) => new PeopleWithTasksModel()
                          { User = u, Tasks = t.ToList() }).
                          OrderBy(obj => obj.User.FirstName).
                          ToList();
        }


        //-----------------------------------------
        // From LINQ lection Task 6
        // Отримати наступну структуру (передати Id користувача в параметри):
        // - User - Останній проект користувача (за датою створення) - Загальна кількість тасків під останнім проектом -
        // Загальна кількість незавершених або скасованих тасків для користувача - Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
        public UserInfoModel GetUserInfo(int id)
        {
            return _dataService.GetUsers().Where(u => u.Id == id).Join(_dataService.GetProjects(),
                             u => u.TeamId,
                             p => p.TeamId,
                             (u, p) => new UserInfoModel()
                             {
                                 User = u,
                                 LastProject = p,
                                 TaskNumberForLastProject = p.Tasks.Count,
                                 UndoneTasksNumber = _dataService.GetTasks().Where(el => (el.PerformerId == u.Id) && (el.FinishedAt == null)).Count(),
                                 LongestTask = _dataService.GetTasks().Where(el => (el.PerformerId == u.Id) && (el.FinishedAt != null)).
                                  OrderByDescending(el => el.FinishedAt - el.CreatedAt).ToList().FirstOrDefault()
                             }).ToList().OrderByDescending(el => el.LastProject.CreatedAt).Take(1).ToList().FirstOrDefault();
        }
    }
}
