﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;
using Projects.BLL.Services;
using Projects.Common.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Projects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        ProjectService projectService;
        public ProjectsController(ProjectService prjService)
        {
            projectService = prjService;
        }

        // GET: api/<ProjectsController>
        [HttpGet]
        public IEnumerable<ProjectModel> Get()
        {
            return projectService.Get();
        }

        // Add Project
        // POST api/<ProjectsController>
        [HttpPost]
        [Route("AddProject")]
        public IActionResult AddProject([FromBody] ProjectDTO projectDTO)
        {
            try
            {
                ProjectModel projectModel = ConverterService.ProjectDTOToModel(projectDTO);
                projectService.Add(projectModel);
                return CreatedAtAction(nameof(AddProject), new { id = projectModel.Id }, projectModel);
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        // PUT api/<ProjectsController>
        [HttpPut]
        public IActionResult Put([FromBody] ProjectDTO projectDTO)
        {
            try
            {
                ProjectModel projectModel = ConverterService.ProjectDTOToModel(projectDTO);
                projectService.Update(projectModel);
                return Ok("Object updated");
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        // DELETE api/<ProjectsController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                projectService.Delete(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // from LINQ lection Task 1
        // Отримати кількість тасків у проекті конкретного користувача 
        // (по id) (словник, де key буде проект, а value кількість тасків).
        [HttpGet("{id}")]
        [Route("GetNumOfTaskByAuthorID/{id}")]
        public IDictionary<int, int> GetNumOfTaskByAuthorID(int id)
        {
            return projectService.GetNumOfTaskByAuthorID(id);    
        }

        // Task 7
        // Отримати таку структуру: Проект - Найдовший таск проекту (за описом) -
        // Найкоротший таск проекту (по імені) - Загальна кількість користувачів в команді проекту,
        // де або опис проекту >20 символів, або кількість тасків <3
        [HttpGet]
        [Route("GetProjectInfo")]
        public IEnumerable<ProjectInfoModel> GetProjectInfo()
        {
            return projectService.GetProjectInfo();
        }
     }
}
