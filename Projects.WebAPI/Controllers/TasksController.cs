﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;
using Projects.Common.Models;
using Projects.BLL.Services;

namespace Projects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        TasksService tasksService;
        public TasksController(TasksService tsService)
        {
            tasksService = tsService;
        }


        // Get all Tasks
        [HttpGet]
        public IEnumerable<TaskModel> Get()
        {
            return tasksService.Get();
        }

        // Add Task
        // POST api/<TasksController>
        [HttpPost]
        [Route("AddTask")]
        public IActionResult AddTask([FromBody] TaskDTO taskDTO)
        {
            try
            {
                TaskModel taskModel = ConverterService.TaskDTOToModel(taskDTO);
                tasksService.Add(taskModel);
                return CreatedAtAction(nameof(AddTask), new { id = taskDTO.Id }, taskModel);
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        // PUT api/<TasksController>
        [HttpPut]
        public IActionResult Put([FromBody] TaskDTO taskDTO)
        {
            try
            {
                TaskModel taskModel = ConverterService.TaskDTOToModel(taskDTO);
                tasksService.Update(taskModel);
                return Ok("Object updated");
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        // DELETE api/<TasksController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete( int id)
        {
            try
            {
                tasksService.Delete(id);
                return NoContent();
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        // From LinQ Task2
        // Отримати список тасків, призначених для конкретного користувача (по id), 
        // де name таска <45 символів (колекція з тасків).
        [HttpGet("{id}")]
        [Route("TaskForPerformer/{id}")]
        public IEnumerable<TaskModel> GetListOfTaskForPerformer(int id)
        {
            return tasksService.GetListOfTaskForPerformer(id);
        }


        // from LinQ Task3
        [HttpGet("{id}")]
        [Route("Finished/{id}")]
        public IEnumerable<IdNameOfTaskModel> GetFinishedTaskByUserID(int id)
        {
            return tasksService.GetFinishedTaskByUserID(id);
        }
    }
}
