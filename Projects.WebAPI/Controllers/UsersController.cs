﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using Projects.WebAPI.Models;
using Projects.DatabaseAccess.Entities;
using Projects.BLL.Services;
using Projects.Common.Models;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Projects.WebAPI.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        UsersService usersService;
        public UsersController(UsersService usService)
        {
            usersService = usService;
        }

        // Get All User
        [HttpGet]
        public IEnumerable<UserModel> GetAllPeople()
        {
            return usersService.Get();
        }

        // Task 5 Linq
        // GET: api/Users/GetPeopleWithTask
        [HttpGet]
        [Route("GetPeopleWithTask")]
        public IEnumerable<PeopleWithTasksModel> GetPeopleWithTask()
        {
            return usersService.GetPeopleWithTask();
        }


        // Task 6 Linq
        [HttpGet]
        [Route("GetUserInfo/{id}")]
        public UserInfoModel GetUserInfo(int id)
        {
            return usersService.GetUserInfo(id);
        }

        // POST api/AddUser
        [HttpPost]
        [Route("AddUser")]
        public IActionResult AddUser([FromBody] UserDTO user)
        {
            try
            {
                UserModel um = ConverterService.UserDTOToModel(user);
                usersService.Add(um);
                return CreatedAtAction(nameof(AddUser), new { id = user.Id }, user);
            } catch
            {
                return BadRequest("invalid input format");
            }
        }

        // PUT api/<Users>
        [HttpPut]
        public IActionResult Update([FromBody] UserDTO userDTO)
        {
            try
            {
                UserModel userModel = ConverterService.UserDTOToModel(userDTO);
                usersService.Update(userModel);
                return Ok("Object updated");
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        // DELETE api/<Users>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                usersService.Delete(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
