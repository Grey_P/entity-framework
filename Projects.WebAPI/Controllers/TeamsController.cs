﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;
using System.Data.Entity.Core;
using Projects.BLL.Services;
using Projects.Common.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Projects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        TeamsService teamsService;
        public TeamsController(TeamsService ts)
        {
            teamsService = ts;
        }

        // GET: api/<TeamsController>
        [HttpGet]
        public IEnumerable<TeamModel> Get()
        {
            return teamsService.Get();
        }

        // Add Team
        // POST api/<TeamsController>/AddTeam
        [ActionName("AddTeam")]
        [Route("AddTeam")]
        [HttpPost]
        public IActionResult AddTeam([FromBody] TeamDTO team)
        {
            try
            {
                TeamModel teamModel = ConverterService.TeamDTOToModel(team);
                teamsService.Add(teamModel);
                return CreatedAtAction(nameof(AddTeam), new { id = team.Id }, team);
            } catch
            {
                return BadRequest("invalid input format");
            }
        }

        // updating Data
        // PUT api/<TeamsController>
        [HttpPut]
        public IActionResult Put([FromBody] TeamDTO teamDTO)
        {
            try
            {
                TeamModel teamModel = ConverterService.TeamDTOToModel(teamDTO);
                teamsService.Update(teamModel);
                return Ok("Object updated");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<TeamsController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                teamsService.Delete(id);
                return NoContent();
            } catch (ObjectNotFoundException)
            {
                return NotFound("There isn`t any teams with this id");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        // 4th task from LinQ
        // Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        // відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        [HttpGet]
        [Route("GetTeamWhereOldPeople")]
        public IEnumerable<TeamModel> GetTeamWhereOld()
        {
            return teamsService.GetTeamPeopleOlderThen10();
        }
    }
}
