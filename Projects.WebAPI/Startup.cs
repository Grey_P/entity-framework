using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.DatabaseAccess.Entities;
using Projects.DatabaseAccess;
using Microsoft.EntityFrameworkCore;
using Projects.DatabaseAccess.Interfaces;
using Projects.DatabaseAccess.Repositories;
using Projects.BLL.Services;

namespace Projects.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

           
            services.AddScoped<IRepository<ProjectModel>, ProjectsRepository>(); 
            services.AddScoped<IRepository<TaskModel>, TasksRepository>(); 
            services.AddScoped<IRepository<TeamModel>, TeamsRepository>(); 
            services.AddScoped<IRepository<UserModel>, UsersRepository>(); 

            //-------------< Added all my services >------------
            services.AddTransient<DataService>();

            services.AddTransient<ProjectService>();
            services.AddTransient<UsersService>();
            services.AddTransient<TasksService>();
            services.AddTransient<TeamsService>();

           
            //---------------------------------------------------
            services.AddControllers();

            services.AddDbContext<ProjectsDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProjectsDBConnectionString")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Projects.WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Projects.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
